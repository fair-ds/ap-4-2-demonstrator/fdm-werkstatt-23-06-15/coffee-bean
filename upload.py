import argparse
import os

import boto3

S3_ENDPOINT = "https://coscine-nrw-s3-01.s3.fds.nrw:9021"

def _parse_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("--filepath", help="The file to upload")
    args = parser.parse_args()
    return args

def upload_file_to_s3(bucket_name, filepath):
    s3 = boto3.client("s3", endpoint_url=S3_ENDPOINT)
    try:
        with open(filepath, "rb") as file:
            s3.upload_fileobj(file, bucket_name, filepath)
        print("File uploaded successfully.")
    except Exception as e:
        print("Error uploading file:", str(e))

if __name__ == "__main__":
    args = _parse_cli()

    upload_file_to_s3(
        bucket_name=os.environ.get("COSCINE_S3_BUCKET"),
        filepath=args.filepath)
